<?php
require 'gamenet-header.php';
?>
<style>
    .categories-widget a {
        padding-left: 0;
        padding-right: 38px;
    }

    .categories-widget h4 {
        padding-left: 0;
        padding-right: 38px;
    }
</style>
<section class="page-top-section set-bg" data-setbg="gamenet/img/page-top-bg/1.jpg">
    <div class="page-info">
        <h2 style="text-align: right;direction: rtl">Games</h2>
        <div class="site-breadcrumb" style="text-align: right;direction: rtl">
            <a href="">Home</a> /
            <span>Games</span>
        </div>
    </div>
</section>


<!-- Games section -->
<section class="games-section" style="direction: rtl;text-align: right">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-8 col-md-7">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="game-item">
                            <img src="gamenet/img/games/1.jpg" alt="#">
                            <h5>Zombie Appocalipse 2</h5>
                            <a href="game-single.html" class="read-more">Read More <img src="gamenet/img/icons/double-arrow.png"
                                                                                        alt="#"/></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="game-item">
                            <img src="gamenet/img/games/2.jpg" alt="#">
                            <h5>Dooms Day</h5>
                            <a href="game-single.html" class="read-more">Read More <img src="gamenet/img/icons/double-arrow.png"
                                                                                        alt="#"/></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="game-item">
                            <img src="gamenet/img/games/3.jpg" alt="#">
                            <h5>The Huricane</h5>
                            <a href="game-single.html" class="read-more">Read More <img src="gamenet/img/icons/double-arrow.png"
                                                                                        alt="#"/></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="game-item">
                            <img src="gamenet/img/games/4.jpg" alt="#">
                            <h5>Star Wars</h5>
                            <a href="game-single.html" class="read-more">Read More <img src="gamenet/img/icons/double-arrow.png"
                                                                                        alt="#"/></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="game-item">
                            <img src="gamenet/img/games/5.jpg" alt="#">
                            <h5>Candy land</h5>
                            <a href="game-single.html" class="read-more">Read More <img src="gamenet/img/icons/double-arrow.png"
                                                                                        alt="#"/></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="game-item">
                            <img src="gamenet/img/games/6.jpg" alt="#">
                            <h5>E.T.</h5>
                            <a href="game-single.html" class="read-more">Read More <img src="gamenet/img/icons/double-arrow.png"
                                                                                        alt="#"/></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="game-item">
                            <img src="gamenet/img/games/7.jpg" alt="#">
                            <h5>Zombie Appocalipse 2</h5>
                            <a href="game-single.html" class="read-more">Read More <img src="gamenet/img/icons/double-arrow.png"
                                                                                        alt="#"/></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="game-item">
                            <img src="gamenet/img/games/8.jpg" alt="#">
                            <h5>Dooms Day</h5>
                            <a href="game-single.html" class="read-more">Read More <img src="gamenet/img/icons/double-arrow.png"
                                                                                        alt="#"/></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="game-item">
                            <img src="gamenet/img/games/9.jpg" alt="#">
                            <h5>The Huricane</h5>
                            <a href="game-single.html" class="read-more">Read More <img src="gamenet/img/icons/double-arrow.png"
                                                                                        alt="#"/></a>
                        </div>
                    </div>
                </div>
                <div class="site-pagination">
                    <a href="#" class="active">01.</a>
                    <a href="#">02.</a>
                    <a href="#">03.</a>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-5 sidebar game-page-sideber">
                <div id="stickySidebar">
                    <div class="widget-item">
                        <div class="categories-widget">
                            <h4 class="widget-title">categories</h4>
                            <ul>
                                <li><a href="">Games</a></li>
                                <li><a href="">Gaming Tips & Tricks</a></li>
                                <li><a href="">Online Games</a></li>
                                <li><a href="">Team Games</a></li>
                                <li><a href="">Community</a></li>
                                <li><a href="">Uncategorized</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget-item">
                        <div class="categories-widget">
                            <h4 class="widget-title">platform</h4>
                            <ul>
                                <li><a href="">Xbox</a></li>
                                <li><a href="">X box 360</a></li>
                                <li><a href="">Play Station</a></li>
                                <li><a href="">Play Station VR</a></li>
                                <li><a href="">Nintendo Wii</a></li>
                                <li><a href="">Nintendo Wii U</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget-item">
                        <div class="categories-widget">
                            <h4 class="widget-title">Genre</h4>
                            <ul>
                                <li><a href="">Online</a></li>
                                <li><a href="">Adventure</a></li>
                                <li><a href="">S.F.</a></li>
                                <li><a href="">Strategy</a></li>
                                <li><a href="">Racing</a></li>
                                <li><a href="">Shooter</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Games end-->


<!-- Featured section -->
<section class="featured-section">
    <div class="featured-bg set-bg" data-setbg="gamenet/img/featured-bg.jpg"></div>
    <div class="featured-box" style="text-align: right;direction: rtl">
        <div class="text-box">
            <div class="top-meta"></div>
            <h3 style="text-align: right;direction: rtl">The game you’ve been waiting for is out now</h3>
            <p style="text-align: right;direction: rtl">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.
                Ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliquamet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua. Vestibulum posuere porttitor justo id pellentesque. Proin id lacus feugiat, posuere erat sit
                amet, commodo ipsum. Donec pellentesque vestibulum metus...</p>
            <a href="#" class="read-more">Read More <img src="gamenet/img/icons/double-arrow.png" alt="#"/></a>
        </div>
    </div>
</section>
<!-- Featured section end-->
<?php
    require 'gamenet-footer.php';
