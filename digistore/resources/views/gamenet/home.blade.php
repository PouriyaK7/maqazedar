<?php
require 'gamenet-header.php';
?>
<section class="hero-section overflow-hidden">
    <div class="hero-slider owl-carousel">
        <div class="hero-item set-bg d-flex align-items-center justify-content-center text-center" data-setbg="gamenet/img/slider-bg-1.jpg">
            <div class="container">
                <h2 style="direction: rtl">Game on!</h2>
                <p style="direction: rtl">Fusce erat dui, venenatis et erat in, vulputate dignissim lacus. Donec vitae tempus dolor,<br>sit amet elementum lorem. Ut cursus tempor turpis.</p>
                <a href="#" class="site-btn">Read More  <img src="gamenet/img/icons/double-arrow.png" alt="#"/></a>
            </div>
        </div>
        <div class="hero-item set-bg d-flex align-items-center justify-content-center text-center" data-setbg="gamenet/img/slider-bg-2.jpg">
            <div class="container">
                <h2 style="direction: rtl">Game on!</h2>
                <p style="direction: rtl">Fusce erat dui, venenatis et erat in, vulputate dignissim lacus. Donec vitae tempus dolor,<br>sit amet elementum lorem. Ut cursus tempor turpis.</p>
                <a href="#" class="site-btn">Read More  <img src="gamenet/img/icons/double-arrow.png" alt="#"/></a>
            </div>
        </div>
    </div>
</section>

	<!-- Hero section end-->


	<!-- Intro section -->
	<section class="intro-section" style="direction: rtl;text-align: right">
		<div class="container">
			<div class="row">
				@foreach($news as $item)
                    <?php $date = explode(' ',$item['date']); ?>
                    <div class="col-md-4">
                        <div class="intro-text-box text-box text-white">
                            <div class="top-meta">{{$date[0]}}  /  <a href="{{URL::to('game/'.$item['game_title'].'/'.$item['game_id'])}}">{{$item['game_title']}}</a></div>
                            <h3>{{$item['news_title']}}</h3>
                            <p>{{substr($item['text'],0,167)}}<?php echo strlen($item['text']) > 166? '...': ''; ?></p>
                            <a href="{{URL::to('game-news/'.$item['news_title'].'/'.$item['news_id'])}}" class="read-more">Read More  <img src="gamenet/img/icons/double-arrow.png" alt="#"/></a>
                        </div>
                    </div>
                @endforeach
			</div>
		</div>
	</section>
	<!-- Intro section end -->


	<!-- Blog section -->
	<section class="blog-section spad" style="direction: rtl;text-align: right">
		<div class="container">
			<div class="row">
				<div class="col-xl-9 col-lg-8 col-md-7">
					<div class="section-title text-white">
						<h2>Top Genres</h2>
					</div>
					<ul class="blog-filter">
                        @foreach($genres as $genre)
                            <li><a href="{{URL::to('games/genre/'.$genre['caption'])}}">{{$genre['caption']}}</a></li>
                        @endforeach
					</ul>
					<!-- Blog item -->
                    @foreach($center_news as $item)
                        <?php $date = explode(' ',$item['date']); ?>
                        <div class="blog-item">
                            <div class="blog-thumb">
                                <img src="gamenet/img/News/{{str_replace(' ','-',$item['news_title'].'-'.$item['news_id'])}}.jpg" alt="اخبار-بازی-{{$item['news_title'].'-'.$item['game_title']}}">
                            </div>
                            <div class="blog-text text-box text-white">
                                <div class="top-meta">{{$date[0]}}  /  <a href="{{URL::to('game-news/'.$item['news_title'].'/'.$item['news_id'])}}">{{$item['game_title']}}</a></div>
                                <h3>{{$item['news_title']}}</h3>
                                <p>{{substr($item['text'],0,167)}}<?php echo strlen($item['text']) > 166? '...': ''; ?></p>
                                <a href="#" class="read-more">Read More  <img src="gamenet/img/icons/double-arrow.png" alt="#"/></a>
                            </div>
                        </div>
                    @endforeach
				</div>
				<div class="col-xl-3 col-lg-4 col-md-5 sidebar">
					<div id="stickySidebar">
						<div class="widget-item">
							<h4 class="widget-title">Trending</h4>
							<div class="trending-widget">
								@foreach($side_news as $item)
                                    <?php $date = explode(' ',$item['date']); ?>
                                    <div class="tw-item">
                                        <div class="tw-thumb">
                                            <img src="gamenet/img/News/{{str_replace(' ','-',$item['news_title'].'-'.$item['news_id'])}}" alt="اخبار-بازی{{$item['news_title'].'-'.$item['game_title']}}">
                                        </div>
                                        <div class="tw-text">
                                            <div class="tw-meta">{{$date[0]}}  /   <a href="{{URL::to('game/'.$item['game_title'].'/'.$item['game_id'])}}">{{$item['game_title']}}</a></div>
                                            <h5>{{$item['news_title']}}</h5>
                                        </div>
                                    </div>
                                @endforeach
							</div>
						</div>
						<div class="widget-item">
							<div class="categories-widget">
								<h4 class="widget-title">Other genres...</h4>
								<ul>
									@foreach($other_genres as $genre)
                                        <li><a href="{{URL::to('games/genres/'.$genre['caption'])}}">{{$genre['caption']}}</a></li>
                                    @endforeach
								</ul>
							</div>
						</div>
						<div class="widget-item">
						<a href="#" class="add">
							<img src="gamenet/img/add.jpg" alt="">
						</a>
					</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Blog section end -->


	<!-- Intro section -->
	<section class="intro-video-section set-bg d-flex align-items-end " data-setbg="gamenet/img/promo-bg.jpg">
		<a href="https://www.youtube.com/watch?v=uFsGy5x_fyQ" class="video-play-btn video-popup"><img src="gamenet/img/icons/solid-right-arrow.png" alt="#"></a>
		<div class="container" style="text-align: right;direction: rtl">
			<div class="video-text">
				<h2>Promo video of the game</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
			</div>
		</div>
	</section>
	<!-- Intro section end -->


	<!-- Featured section -->
	<section class="featured-section">
		<div class="featured-bg set-bg" data-setbg="gamenet/img/featured-bg.jpg"></div>
		<div class="featured-box" style="text-align: right;direction: rtl">
			<div class="text-box">
				<div class="top-meta"></div>
				<h3><?php $game = $latest_game[0]; ?>
                    {{$game['title']}}</h3>
				<p>{{$game['description']}}</p>
				<a href="#" class="read-more">Read More  <img src="gamenet/img/Games/{{$game['title'].'/'.$game['id']}}" alt="بازی-بازی-کامپیوتری-بازی-پلی-استیشن-گیم-نت-دیجی-استور-{{$game['title'].'-'.str_replace(' ','-',$game['description'])}}"/></a>
			</div>
		</div>
	</section>
<?php
    require 'gamenet-footer.php';
