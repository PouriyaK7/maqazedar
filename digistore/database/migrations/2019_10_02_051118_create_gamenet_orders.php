<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamenetOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gamenet_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('console');
            $table->float('game_time');
            $table->integer('per_hour');
            $table->integer('gamepad_count');
            $table->integer('final_price');
            $table->integer('office');
            $table->integer('gamepad_fall')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gamenet_orders');
    }
}
