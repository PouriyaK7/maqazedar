<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDigistoreUniqueUserColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('username')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->dropColumn('name');
            $table->integer('type')->default(0);
            $table->string('phone_number');
            $table->integer('office')->nullable();
            $table->dateTime('verification')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name');
            $table->dropColumn('username');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('type');
            $table->dropColumn('phone_number');
            $table->dropColumn('office');
            $table->dropColumn('verification');
        });
    }
}
