<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeColumnsToGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->float('price_rate');
            $table->float('graphics');
            $table->float('game_play');
            $table->float('song');
            $table->float('dubbing');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->dropColumn('price_rate');
            $table->dropColumn('graphics');
            $table->dropColumn('game_play');
            $table->dropColumn('song');
            $table->dropColumn('dubbing');
        });
    }
}
