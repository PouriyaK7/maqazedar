<section class="newsletter-section">
    <div class="container">
        <h2 style="direction: rtl">Subscribe to our newsletter</h2>
        <form class="newsletter-form">
            <input type="text" style="direction: rtl" placeholder="ENTER YOUR E-MAIL">
            <button class="site-btn">subscribe  <img src="gamenet/img/icons/double-arrow.png" alt="#"/></button>
        </form>
    </div>
</section>
<!-- Newsletter section end -->


<!-- Footer section -->
<footer class="footer-section">
    <div class="container">
        <!--
        <div class="footer-left-pic">
            <img src="" alt="">
    </div>
    <div class="footer-right-pic">
        <img src="gamenet/img/footer-right-pic.png" alt="">
    </div>
        -->
        <a href="#" class="footer-logo">
            <img src="gamenet/img/logo.png" alt="">
        </a>
        <ul class="main-menu footer-menu">
            <li><a href="">Home</a></li>
            <li><a href="">Games</a></li>
            <li><a href="">Reviews</a></li>
            <li><a href="">News</a></li>
            <li><a href="">Contact</a></li>
        </ul>
        <div class="footer-social d-flex justify-content-center">
            <a href="#"><i class="fa fa-pinterest"></i></a>
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-dribbble"></i></a>
            <a href="#"><i class="fa fa-behance"></i></a>
        </div>
        <div class="copyright"><a href="">Colorlib</a> 2018 @ All rights reserved</div>
    </div>
</footer>
<!-- Footer section end -->


<!--====== Javascripts & Jquery ======-->
<script src="gamenet/js/jquery-3.2.1.min.js"></script>
<script src="gamenet/js/bootstrap.min.js"></script>
<script src="gamenet/js/jquery.slicknav.min.js"></script>
<script src="gamenet/js/owl.carousel.min.js"></script>
<script src="gamenet/js/jquery.sticky-sidebar.min.js"></script>
<script src="gamenet/js/jquery.magnific-popup.min.js"></script>
<script src="gamenet/js/main.js"></script>

</body>
</html>
