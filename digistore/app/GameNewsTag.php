<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameNewsTag extends Model
{
    protected $fillable = ['game_news', 'tag'];
    protected $primaryKey = ['game_news', 'tag'];
    public $timestamps = false;
    protected $table = 'game_news_tags';
    public $incrementing = false;
}
