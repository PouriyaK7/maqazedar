<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $table = 'product_types';
}
