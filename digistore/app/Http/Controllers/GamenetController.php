<?php

namespace App\Http\Controllers;

use App\Console;
use App\ConsolePrice;
use App\Game;
use App\GamenetGame;
use App\GamenetOrder;
use App\GameNews;
use App\GameNewsTag;
use App\Gamer;
use App\GamerGame;
use App\Genre;
use App\Office;
use App\Platform;
use App\Tournament;
use App\TournamentGamer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GamenetController extends Controller
{
    public function games_page(){
        $games = Game::paginate(9);
        $genres = Genre::take(5)
            ->get();
        $platforms = Platform::take(6)
            ->get();
        $last_game = Game::take(1)
            ->orderBy('games.id', 'DESC')
            ->where('games.published','!=', null)
            ->get();
        return view('gamenet.', compact('games', 'last_game', 'genres', 'platforms'));
    }

    public function single_game($title, $id){
        $games = Game::join('users', function ($join){
            $join->on('users.id', '=', 'games.testimonial_author');
        })
            ->join('genres', function ($join){
                $join->on('genres.id','=','games.genre');
            })
            ->where('games.published','!=',null)
            ->where('games.id','=',$id)
            ->get([
                '*',
                'users.id AS user_id',
                'genres.id AS genre_id',
                'games.id AS game_id'
            ]);
        /*$last_game = Game::take(1)
            ->orderBy('games.id','DESC')
            ->where('games.published','!=',null)
            ->get();
        */
        return view('gamenet.', compact('games'));
    }

    public function news_page(){
        $news = GameNews::select([
            '*',
            'games.id AS game_id',
            'game_news.id AS news_id',
            'game_news.author AS news_author',
            'game_news.title AS news_title',
            'games.title AS game_title'
        ])
            ->join('games', function ($join){
                $join->on('games.id','=','game_news.game');
            })
            ->paginate(3);
        $latest_games = Game::take(4)
            ->where('games.published','!=', null)
            ->orderBy('games.id','DESC')
            ->get();
        $genres = Genre::take(6)
            ->get();
        return view('gamenet.',compact('news','latest_games','genres'));
    }

    public function news_single($title, $id){
        $news = GameNews::join('games',function ($join){
            $join->on('games.id','=','game_news.game');
        })
            ->where('game_news.id', '=', $id)
            ->get([
                '*',
                'games.id AS game_id',
                'game_news.id AS news_id',
                'game_news.author AS news_author',
                'game_news.title AS news_title',
                'games.title AS game_title'
            ]);
        return view('gamenet.',compact('news'));
    }

    public function game_home(){
        $news = GameNews::leftJoin('games', function ($join){
            $join->on('games.id','=','game_news.game');
        })
            ->take(3)
            ->get([
                '*',
                'games.id AS game_id',
                'game_news.id AS news_id',
                'game_news.author AS news_author',
                'game_news.title AS news_title',
                'games.title AS game_title'
            ]);
        $genres = Genre::where('top_genre','=',1)
            ->get();
        $side_news = GameNews::leftJoin('games', function ($join){
            $join->on('games.id','=','game_news.game');
        })
            ->skip(3)
            ->take(4)
            ->get([
                '*',
                'games.id AS game_id',
                'game_news.id AS news_id',
                'game_news.author AS news_author',
                'game_news.title AS news_title',
                'games.title AS game_title'
            ]);
        $center_news = GameNews::leftJoin('games', function ($join){
            $join->on('games.id', '=', 'game_news.game');
        })
            ->skip(7)
            ->take(3)
            ->get([
                '*',
                'games.id AS game_id',
                'game_news.id AS news_id',
                'game_news.author AS news_author',
                'game_news.title AS news_title',
                'games.title AS game_title'
            ]);
        $latest_game = Game::take(1)
            ->orderBy('games.id','DESC')
            ->where('published','!=',null)
            ->get();
        $other_genres = Genre::skip(4)
            ->take(6)
            ->get();
        return view('gamenet.home', compact('genres', 'center_news', 'latest_game', 'news', 'side_news', 'other_genres'));
    }

    public function tournament_single($title, $id){
        $tournament_gamers = Tournament::join('tournament_gamers', function ($join){
            $join->on('tournament_gamers.tournament','=','tournaments.id');
        })
            ->join('gamers', function ($join){
                $join->on('gamers.id','=','tournament_gamers.gamer');
            })
            ->where('tournaments.sign_start_date','>=',Carbon::now())
            ->where('tournaments.verify','=',1)
            ->get([
                '*',
                'gamers.id AS gamer_id',
                'tournaments.id AS tour_id',
            ]);
        $tournaments = Tournament::join('offices', function ($join){
            $join->on('offices.id','=','tournaments.office');
        })
            ->join('games', function ($join){
                $join->on('games.id','=','tournaments.game');
            })
            ->where('tournaments.sign_start_date','>=',Carbon::now())
            ->where('tournaments.verify','=',1)
            ->get([
                '*',
                'tournaments.id AS tour_id',
                'games.id AS game_id',
                'offices.id AS office_id',
                'games.title AS game_title',
                'tournaments.title AS tour_title',
                'games.description AS game_description',
                'tournaments.description AS tour_description'
            ]);
    }

    public function gamer_single($name, $id){
        $gamer_games = GamerGame::join('games', function ($join){
            $join->on('games.id', '=', 'gamer_games.game');
        })
            ->join('gamers', function ($join){
                $join->on('gamers.id', '=', 'gamer_games.gamer');
            })
            ->where('gamers.id', '=', $id)
            ->get([
                '*',
                'gamers.id AS gamer_id',
                'games.id AS game_id',
            ]);
        $gamers = Gamer::where('id', '=', $id)
            ->get();
        $gamer_tournaments = TournamentGamer::join('tournaments', function ($join){
            $join->on('tournaments.id', '=', 'tournament_gamers.tournament');
        })
            ->join('gamers', function ($join){
                $join->on('gamers.id', '=', 'tournament_gamers.gamer');
            })
            ->get([
                '*',
                'gamers.id AS gamer_id',
                'tournaments.id AS tour_id'
            ]);
    }

    public function single_office($title, $id){
        $offices = Office::join('gamenet_games', function ($join){
            $join->on('gamenet_games.gamenet', '=', 'offices.id');
        })
            ->join('games', function ($join){
                $join->on('games.id', '=', 'gamenet_games.game');
            })
            ->where('offices.id', '=', $id)
            ->get([
                '*',
                'offices.id AS office_id',
                'games.id AS game_id',
            ]);
        $gamers = Office::join('gamers', function ($join){
            $join->on('gamers.favourite_gamenet', '=', 'offices.id');
        })
            ->where('offices.id', '=', $id)
            ->get([
                '*',
                'offices.id AS office_id',
                'gamers.id AS gamer_id'
            ]);
    }

    public function add_game(Request $request){
        if (Auth::check()){
            if (Auth::user()->type == 22){
                $inputs = $request->except('_token');
                Game::create(
                    ['title' => $inputs['title']],
                    ['author' => Auth::id()],
                    ['description' => $inputs['description']],
                    ['game_link' => $inputs['game_link']],
                    ['video' => $inputs['video']],
                    ['vip' => $inputs['vip'] ?? 0],
                    ['platform' => $inputs['platform']],
                    ['price' => $inputs['price']],
                    ['genre' => $inputs['genre']],
                    ['published' => $inputs['published'] ?? 0],
                    ['price_rate' => $inputs['price_rate']],
                    ['graphics' => $inputs['graphics']],
                    ['game_play' => $inputs['game_play']],
                    ['song' => $inputs['song']],
                    ['dubbing' => $inputs['dubbing']],
                    ['testimonials' => $inputs['testimonials']],
                    ['testimonial_author' => Auth::id()]
                );
            }
        }
    }

    public function add_game_news(Request $request){
        if (Auth::check()){
            if (Auth::user()->type == 22){
                $inputs = $request->except('_token');
                GameNews::create(
                    ['text' => $inputs['text']],
                    ['author' => Auth::id()],
                    ['title' => $inputs['title']],
                    ['game' => $inputs['game'] ?? 0]
                );
            }
        }
    }

    public function add_game_to_gamenets($type, $gamenet, $game){
        if (Auth::check()){
            if (Auth::user()->office === $gamenet){
                switch ($type){
                    case 'add':
                        GamenetGame::firstOrCreate(
                            ['gamenet' => Auth::user()->office],
                            ['game' => $game]
                        );
                        break;
                    case 'remove':
                        GamenetGame::where('gamenet', '=', Auth::user()->office)
                            ->where('game', '=', $game)
                            ->delete();
                        break;
                }
            }
        }
    }

    public function add_gamer_to_tournaments($gamer, $tournament){

    }

    public function gamer_login(Request $request){
    }

    public function gamer_register(){

    }

    // Gamenet management

    public function add_order(Request $request){
        if (Auth::check()){
            if (Auth::user()->office != null){
                $inputs = $request->except('_token');
                $verify = Console::where('id', '=', $inputs['console'])
                    ->where('office', '=', Auth::user()->office)
                    ->get();
                if (count($verify) == 1){
                    $prices = ConsolePrice::where('console', '=', $inputs['console'])
                        ->get();
                    $price = $prices[0];
                   $prices = $price[$inputs['gamepad_count'].'g'];
                    GamenetOrder::create(
                        ['console' => $inputs['console']],
                        ['game_time' => $inputs['game_time']],
                        ['per_hour' => $prices],
                        ['gamepad_count' => $inputs['gamepad_count']],
                        ['final_price' => $inputs['game_time'] * $prices],
                        ['office' => Auth::user()->office],
                        ['gamepad_fall' => $inputs['gamepad_fall'] != 0? 1: null]
                    );
                }
            }
        }
    }

    public function show_orders(){
        if (Auth::check()){
            if (Auth::user()->office != null){
                $orders = GamenetOrder::where('office', '=', Auth::user()->office)
                    ->paginate(30);
            }
        }
    }

    public function show_consoles(){
        if (Auth::check()){
            if (Auth::user()->office != null){
                $consoles = Console::where('office', '=', Auth::user()->office)
                    ->get();
            }
        }
    }

    public function add_tournament(Request $request){
        if (Auth::check()){
            if (Auth::user()->office != null){
                $inputs = $request->except('_token');
                Tournament::create(
                    ['title' => $inputs['title']],
                    ['office' => Auth::user()->office],
                    ['game' => $inputs['game']],
                    ['sign_start_date' => $inputs['sign_start_date']],
                    ['sign_end_date' => $inputs['sign_end_date']],
                    ['start_date' => $inputs['start_date']],
                    ['end_date' => $inputs['end_date']],
                    ['description' => $inputs['description']]
                );
            }
        }
    }
}
