<?php

namespace App\Http\Controllers;

use App\Game;
use App\GameNews;
use App\Genre;
use App\Office;
use App\OfficeJob;
use App\Platform;
use Illuminate\Http\Request;

class DigiStoreController extends Controller
{
    public function home(){
        return redirect('game-net');
    }

    public function show(){
        return view('gamenet.blog');
    }

    public function add_office(Request $request){
        $inputs = $request->except('_token');
        $office = Office::create(
            ['name' => $inputs['name']],
            ['address' => $inputs['address']],
            ['phone_number' => $inputs['phone']],
            ['office_number' => $inputs['office']]
            //['location' => $inputs['location']]
        );
        $jobs = explode('--', $inputs['jobs']);
        foreach ($jobs as $job) OfficeJob::firstOrCreate(['office' => $office->id],['job' => $job]);
    }

}
