<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $table = 'tags';
}
