<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameNews extends Model
{
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $table = 'game_news';
}
