<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficeJob extends Model
{
    protected $fillable = ['office', 'job'];
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = ['office', 'job'];
    protected $table = 'office_jobs';
}
