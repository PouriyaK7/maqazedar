<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsolePrice extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'console',
        '1g',
        '2g',
        '3g',
        '4g'
    ];
    protected $table = 'console_prices';
    protected $primaryKey = 'console';
    public $incrementing = false;
}
