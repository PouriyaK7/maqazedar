<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GamenetGame extends Model
{
    public $timestamps = false;
    protected $table = 'tournament_gamers';
    protected $fillable = ['gamenet', 'game'];
    public $incrementing = false;
    protected $primaryKey = ['gamenet', 'game'];
}
