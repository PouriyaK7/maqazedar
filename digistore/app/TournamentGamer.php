<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentGamer extends Model
{
    protected $fillable = ['gamer', 'tournament'];
    protected $primaryKey = ['gamer', 'tournament'];
    public $timestamps = false;
    protected $table = 'tournament_gamers';
    public $incrementing = false;
}
