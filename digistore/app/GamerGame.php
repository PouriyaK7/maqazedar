<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GamerGame extends Model
{
    protected $fillable = ['gamer', 'game'];
    protected $primaryKey = ['gamer', 'game'];
    public $timestamps = false;
    protected $table = 'gamer_games';
    public $incrementing = false;
}
