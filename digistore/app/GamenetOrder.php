<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GamenetOrder extends Model
{
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $table = 'gamenet_orders';
}
