<?php
require '../__include/__lib.php';
if (isset(
    $_POST['username'],
    $_POST['password']
) && @$_POST['username'] != '' && @$_POST['password'] != ''){
    $login = Lib::login($_POST['username'], $_POST['password']);
    if ($login == 1){
        if (isset($_POST['remember']) && @$_POST['remember'] == 1){
            Lib::set_cookie($_POST['username']);
        }
        header('location: /');
    }
    elseif($login == 0){
        header('location: /Login?error=0');
    }
    else{
        header('location: /Login?error=2');
    }
}
else{
    header('location: /Login?error=1');
}
