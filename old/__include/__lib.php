<?php
session_start();
require '__config.php';

class Lib{
    private static $DB;

    public static function DB() : mysqli {
        if (!isset(self::$DB)){
            self::$DB = new mysqli(Host,User,Password,DB);
            self::$DB->set_charset('utf8');
        }
        return self::$DB;
    }

    public static function add_product($name,$type,$price,$desc,$count,$size = [],$model = 0){
        if (isset($_SESSION['id'])){
            if (isset($_SESSION['office'])){
                $s = $size != [] ? 1 : 0;
                $stmt = Lib::DB()->prepare('INSERT INTO `products`(`name`,`type`,`price`,`user`,`description`,`model`,`office`,`size`,`product_count`) VALUES (?,?,?,?,?,?,?,?,?)');
                $stmt->bind_param('siiisiiii',$name,$type,$price,$_SESSION['id'],$desc,$model,$_SESSION['office'],$s,$count);
                $stmt->execute();
                $product_insert_id = $stmt->insert_id;
                if ($size != []){
                    for ($i=0;$i<count($size);$i++){
                        $stmt = Lib::DB()->prepare('INSERT INTO `product_sizes`(`size`,`product`) VALUES (?,?)');
                        $stmt->bind_param('ii',$size[$i],$product_insert_id);
                        $stmt->execute();
                    }
                }
            }
        }
    }
    public static function main_feature($feature){
        switch ($feature){
            case 1:
                $stmt = Lib::DB()->prepare('SELECT * FROM `products` INNER JOIN `types` ON `types`.`id` = `products`.`type` WHERE `products`.`office` = ? GROUP BY `products`.`id`');
                $stmt->bind_param('i',$_SESSION['office']);
                $stmt->execute();
                $consoles = $stmt->get_result();
                // TODO
                break;
            case 2:
                break;
            case 3:
                break;
        }
    }

    public static function login($username,$password){
        $stmt = Lib::DB()->prepare('SELECT * FROM `users` WHERE `username` = ? OR `email` = ?');
        $user = strtolower($username);
        $stmt->bind_param('ss',$user,$username);
        $stmt->execute();
        $users = $stmt->get_result();
        $user = $users->fetch_assoc();
        if (password_verify($password,$user['password']) && $users->num_rows == 1){
            unset($user['password']);
            $now = new DateTime();
            $date = new DateTime($user['verified'] != null ? $user['verified'] : '');
            if ($date > $now){
                Lib::put_session($user['id'],$user['first_name'],$user['last_name'],$user['username'],$user['email'],$user['phone_number'],$user['type'],$user['office'],$user['verified']);
                return 1;
            }
            else {
                return 0;
            }
        }
        else{
            return -1;
        }
    }

    public static function register($username,$first_name,$last_name,$phone,$email,$pass){
        if (isset(
            $username,
            $pass,
            $first_name,
            $last_name,
            $phone,
            $email
        )){
            $password = password_hash($pass,PASSWORD_DEFAULT);
            $user = strtolower($username);
            $stmt = Lib::DB()->prepare('INSERT INTO `users`(`username`,`first_name`,`last_name`,`phone_number`,`email`,`password`) VALUES (?,?,?,?,?,?)');
            $stmt->bind_param('ssssss',$user,$first_name,$last_name,$phone,$email,$password);
            if ($stmt->execute()){
                Lib::put_session($stmt->insert_id,$first_name,$last_name,$username,$email,$phone,0,0,0);
                // TODO
                return 1;
            }
            else{
                return 0;
            }
        }
        else{
            return -1;
        }
    }

    public static function logout(){
        session_destroy();
        Lib::remove_cookie();
    }

    public static function is_login(){
        return isset($_SESSION['id']) ? 1 : 0;
    }

    public static function add_order($product,$count,$size){
        if (isset(
            $product,
            $count,
            $size
        )){
            $stmt = Lib::DB()->prepare('SELECT * FROM `products` WHERE `id` = ? AND `office` = ?');
            $stmt->bind_param('ii',$product,$_SESSION['office']);
            $stmt->execute();
            if ($stmt->num_rows == 1){
                // Get order details
                $products = $stmt->get_result();
                $products = $products->fetch_assoc();
                $price = $products['price'];
                $full_price = $price * $count;
                // Inserting order to DB
                $stmt = Lib::DB()->prepare('INSERT INTO `orders`(`office`,`product`,`price`,`count`,`full_price`,`order_size`) VALUES (?,?,?,?,?,?)');
                $stmt->bind_param('iiiiii',$_SESSION['office'],$product,$price,$count,$full_price,$size);
                // Update product
                $stmt = Lib::DB()->prepare('UPDATE `products` SET `product_count` = `product_count` - ? WHERE `id` = ?');
                $stmt->bind_param('ii',$count,$product);
                $stmt->execute();
            }
        }
    }

    public static function show_office_orders(){
        $stmt = Lib::DB()->prepare('SELECT *, SUM(`full_price`) AS `full_price_sum`, SUM(`buy_price`) AS `buy_price_sum`,`orders`.id AS `order_id`, `products`.id AS `produc_id` FROM `orders` INNER JOIN `products` ON `products`.`id` = `orders`.product INNER JOIN `sizes` ON `sizes`.id = `orders`.order_size WHERE `orders`.office = ? GROUP BY `orders`.id');
        $stmt->bind_param('i',$_SESSION['office']);
        $stmt->execute();
        return $stmt->get_result();
    }

    public static function show_office_products(){
        // Send sizes if exists for js
        $stmt = Lib::DB()->prepare('SELECT * FROM `product_sizes` INNER JOIN `products` ON `products`.id = `product_sizes`.product INNER JOIN `sizes` ON `sizes`.id = `product_sizes`.size WHERE `products`.office = ? GROUP BY `products`.id');
        $stmt->bind_param('i',$_SESSION['office']);
        $stmt->execute();
        $sizes = $stmt->get_result();
        $is_sizes_exists = $stmt->num_rows;
        // Get products
        $stmt = Lib::DB()->prepare('SELECT * FROM `products` WHERE `office` = ?');
        $stmt->bind_param('i',$_SESSION['office']);
        $stmt->execute();
        return [
            'is_size' => $is_sizes_exists,
            'sizes' => $sizes,
            'products' =>$stmt->get_result()
        ];
        // TODO
    }

    public static function edit_product($id,$name,$price,$desc,$type,$count,$size = [],$model = 0){
        $stmt = Lib::DB()->prepare('SELECT * FROM `products` INNER JOIN `offices` ON `offices`.id = `products`.office WHERE `offices`.id = ?');
        $stmt->bind_param('i',$_SESSION['office']);
        $stmt->execute();
        if ($stmt->num_rows == 1){
            $s = $size != [] ? 1 : $size;
            $stmt = Lib::DB()->prepare('UPDATE `products` SET `name` = ?, `price` = ?, `description` = ?, `type` = ?, `product_count` = ?, size = ?, model = ? WHERE `id` = ?');
            $stmt->bind_param('sisiiiii',$name,$price,$desc,$type,$count,$s,$model,$id);
            $stmt->execute();
            if ($s != 0){
                for ($i=0;$i<count($size);$i++){
                    // Delete all sizes
                    $stmt = Lib::DB()->prepare('DELETE FROM `product_sizes` WHERE `product` = ?');
                    $stmt->bind_param('i',$id);
                    $stmt->execute();
                    // Add new sizes (or even existed)
                    $stmt = Lib::DB()->prepare('INSERT INTO `product_sizes` VALUES (?,?)');
                    $stmt->bind_param('ii',$id,$size[$i]);
                    $stmt->execute();
                }
            }
        }
    }

    public static function delete_product($id){
        // Delete product
        $stmt = Lib::DB()->prepare('DELETE FROM `products` WHERE `id` = ?');
        $stmt->bind_param('i',$id);
        $stmt->execute();
        // Delete product sizes
        $stmt = Lib::DB()->prepare('DELETE FROM `product_sizes` WHERE `product` = ?');
        $stmt->bind_param('i',$id);
        $stmt->execute();
    }

    public static function set_cookie($username){
        setcookie('username', $username, time() + (86400 * 7), '/');
    }

    public static function remove_cookie(){
        setcookie('username',null, time() - 3600);
    }

    public static function cookie_to_session(){
        $username = strtolower($_COOKIE['username']);
        $stmt = Lib::DB()->prepare('SELECT * FROM `users` WHERE `username` = ?');
        $stmt->bind_param('s',$username);
        $stmt->execute();
        $users = $stmt->get_result();
        $user = $users->fetch_assoc();
        Lib::put_session($user['id'],$user['first_name'],$user['last_name'],$user['username'],$user['email'],$user['phone_number'],$user['type'],$user['office'],$user['verified']);
    }

    public static function put_session($id,$first_name,$last_name,$username,$email,$phone,$type,$office,$verified){
        $_SESSION = [
            'id' => $id,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'username' => $username,
            'email' => $email,
            'phone' => $phone,
            'type' => $type,
            'office' => $office,
            'verified' => $verified
        ];
    }
}